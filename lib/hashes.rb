# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_lengths_hash = {}
  str.split(" ").each { |word| word_lengths_hash[word] = word.length }
  word_lengths_hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  sorted = hash.sort_by { |k,v| v }
  sorted.last[0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each { |key, value| older[key] = newer[key] }
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letters = {}
  word.each_char { |chr| letters[chr] = word.count(chr) }
  letters
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  uniq_hash = {}
  arr.each { |el| uniq_hash[el] = nil }
  uniq_hash.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  evens_odds = { even: 0, odd: 0}
  numbers.each do |number|
    evens_odds[:even] += 1 if number.even?
    evens_odds[:odd] += 1 if number.odd?
  end
  evens_odds
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = ["a", "e", "i", "o", "u"]
  vowels_hash = Hash.new(0)

  string.each_char do |chr|
    vowels_hash[chr] += 1 if vowels.include?(chr)
  end

  greatest_key_by_val(vowels_hash)
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  students_late_birthdays = (students.select { |key, value| value > 6 }).keys
  combinations = []

  students_late_birthdays.each_with_index do |student, idx|
    j = idx + 1
    while j < students_late_birthdays.length
      combinations << [student, students_late_birthdays[j]]
      j += 1
    end
  end

  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  species = Hash.new(0)
  specimens.each { |animal| species[animal] += 1 }

  number_of_species = species.length
  smallest_population_size = species.values.min
  largest_population_size = species.values.max

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_counts = character_count(normal_sign.downcase)
  vandalized_sign_counts = character_count(vandalized_sign.downcase)

  vandalized_sign_counts.all? do |char, count|
    normal_sign_counts[char] >= vandalized_sign_counts[char]
  end
end

def character_count(str)
  char_counts = Hash.new(0)
  str.each_char { |chr| char_counts[chr] += 1 }
  char_counts
end
